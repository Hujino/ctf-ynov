#!/bin/sh

docker-compose build

docker network create web

docker volume create nginxdata

docker-compose up -d