Building the images for the first time

```
docker-compose build
```

Creating network

```
docker network create web
```

Creating data persistance

```
docker volume create pgdata
```

Inspect volume

```
docker volume inspect pgdata
```

Starting the images

```
docker-compose up -d
```

Down docker
```
docker-compose down
```

Down docker with volumes
```
docker-compose down -v
```

How to know IP Address ?

```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name
```