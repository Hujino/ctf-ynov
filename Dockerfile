FROM alpine:latest

RUN apk add bash
RUN apk add --update nginx

RUN mkdir -p /var/log/nginx
RUN mkdir -p /var/www/html

COPY nginx_config/default.conf /etc/nginx/conf.d/default.conf
COPY nginx_config/nginx.conf /etc/nginx/nginx.conf

COPY . .

COPY dist /var/www/html/

ENV babeau
ENV J<3largent$!
# make all files belong to the nginx user
RUN chown nginx:nginx /var/www/html

# start nginx and keep the process from backgrounding and the container from quitting
CMD ["nginx", "-g", "daemon off;"]%
